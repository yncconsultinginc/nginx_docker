FROM nginx:mainline-alpine

#USER ROOT

LABEL io.k8s.description="NGINX HOME" \
      io.k8s.display-name="NGINX HOME" \
      io.openshift.expose-services="8081:http" \
      io.openshift.tags="builder,python,nginx" \
      io.openshift.s2i.scripts-url="image:///opt/app-root/s2i/bin"

# support running as arbitrary user which belogs to the root group
RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx
RUN chgrp -R root /var/cache/nginx

# users are not allowed to listen on priviliged ports
RUN sed -i.bak 's/listen\(.*\)80;/listen 8081;/' /etc/nginx/conf.d/default.conf
COPY default.conf /tmp       
RUN cat /tmp/default.conf > /etc/nginx/conf.d/default.conf
EXPOSE 8081

# comment user directive as master process is run as user in OpenShift anyhow
RUN sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf
RUN sed -i.bak 's/^#gzip/gzip/' /etc/nginx/nginx.conf

RUN addgroup nginx root
USER nginx

